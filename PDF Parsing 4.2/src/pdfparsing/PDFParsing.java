package pdfparsing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class PDFParsing 
{

	MongoClient mongoClient;
	DB crawler;
	DBCollection crawldata;
	DBCollection sampleCrawldata;
	public static DBCollection geodata;
	DBCollection titleDescription;
	
	PDFParsing()
	{
		mongoClient = new MongoClient("178.32.49.5", 27017);
		crawler = mongoClient.getDB("crawler");
		crawldata = crawler.getCollection("crawldata");
		sampleCrawldata = crawler.getCollection("sampleCrawldata");	
		geodata=crawler.getCollection("geodata");
		titleDescription=crawler.getCollection("titleDescription");
	}
	
	BasicDBObject parsePDF(String link)
	{
		BodyContentHandler handler;
		Metadata metadata;
		InputStream inputstream;
		
		String pdfContent;
		String[] splittedPdfContent;
				
		ParseContext pcontext;
		PDFParser pdfparser;
		
		String title = "";
		String authorityTitle="";
		String mainContent="";
		String description="";
		String location=""; 
		String leadDate=""; 
		String personName=""; 
		String companyName="";
		String phones="";
		String mails="";
		String[] geoData = null;
		
		Map<String,String> result=new LinkedHashMap<String,String>();
		HashMap hm;
		StanFordNER stanford = new StanFordNER();
		BasicDBObject insertObject = new BasicDBObject();
		DBObject dbObject = null;
		try 
		{
			handler = new BodyContentHandler();
			metadata = new Metadata();
			try
			{
				inputstream = new URL(link).openStream();
				pcontext = new ParseContext();
				pdfparser = new PDFParser();// creating pdf parser
				pdfparser.parse(inputstream, handler, metadata, pcontext);// parsing pdf and getting content in handler

				pdfContent = handler.toString();	// getting the whole content from handler
				pdfContent=pdfContent.replaceAll("\n"," ").replaceAll(" +", " ").trim();
				
				splittedPdfContent = pdfContent.split(" ");

				
				//Title
				if(splittedPdfContent.length > 25)
				{
					for (int i = 0; i <= 25; i++) // getting the first 25 words as title content one by one considering the spaces
					{					
						title = title + splittedPdfContent[i] + " ";
					}
				}
				else
				{
					for (int i = 0; i < splittedPdfContent.length; i++) // getting the all words as title content one by one considering the spaces
					{					
						title = title + splittedPdfContent[i] + " ";
					}
				}
				
				title = title.replaceAll("[^a-zA-Z ]", "").trim();
				
				//Authority Title
				authorityTitle=title;
				
				//Description
				description=pdfContent.replaceAll("[^0-9A-Za-z ]", " ").replaceAll(" +"," ").trim();
				description=StringUtils.stripAccents(description);
							
				//MainContent
				mainContent=description;
				
				//Contact Info			
				phones = getPhone(pdfContent);
				mails = getEmailId(pdfContent);
							
				//NER
				if(!pdfContent.trim().equals(""))
				{	
					hm = stanford.getDataForAllClasses(WordUtils.capitalize(pdfContent.toLowerCase()));
				
					leadDate = hm.get("date").toString();
					location = hm.get("location").toString();
					personName = hm.get("person").toString();		
					companyName = hm.get("organization").toString();
					
					if (leadDate.length() > 2)
						leadDate = leadDate.substring(1, leadDate.length() - 1);
					else
						leadDate = "";

					if (location.length() > 2)
						location = location.substring(1, location.length() - 1);
					else
						location = "";

					if (personName.length() > 2)
						personName = personName.substring(1, personName.length() - 1);
					else
						personName = "";

					if (companyName.length() > 2)
						companyName = companyName.substring(1, companyName.length() - 1);
					else
						companyName = "";
				}
				
				//Geo Location
				geoData=GeoLocation.updateLocation(location);
							
				dbObject = (DBObject) JSON.parse(geoData[1]);
				
				//Set values to Bean		
				result.put("title",title);
				result.put("description",description);
				result.put("authorityTitle",authorityTitle);
						
				result.put("mainContent",mainContent);	
				result.put("companyEmail",mails.trim());
				result.put("companyNumber",phones.trim());	
				
				result.put("leadDate", leadDate.trim());
				result.put("location", location.trim());
				result.put("companyName", companyName.trim());
				result.put("personName", personName.trim());
				
				result.put("geoLocation", geoData[0]);
				result.put("relatedWebsites", "");
						
				//Set Status
				result.put("translationStatus", CheckForTranslation.detectLanguage(mainContent));
				result.put("authorityStatus", "true");
				result.put("boilerpipeStatus", "true");
				result.put("nerStatus", "true");
				result.put("status", "true");
				result.put("updatedBy", "PDFParser");
						
				if(!result.get("description").toString().trim().equals(""))
				{
					for (Map.Entry<String, String> entry : result.entrySet()) {
						System.out.println(entry.getKey() + " =" + entry.getValue());
					}
				}
				else
				{
					System.out.println("PDF is Containing images Only");
				}	
				
				// Add all bean fields.
				insertObject.putAll(result);
				insertObject.put("geoip",dbObject);
			}
			catch(IOException e)
			{
				insertObject.put("authorityStatus","exception");				
				System.err.println("IOException while parsing PDF");
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("Error acccured while parsing PDF");
		}		
		
		result.clear();		
		return insertObject;
	}	


	public String getEmailId(String textProcess)
	{
		textProcess=textProcess.replace("[at]", "@").replace("(at)", "@").replace(" @ ", "@");
		String emailId="";
		Set<String> email = new LinkedHashSet<String>();
		try
		{
			Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+ *?@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(textProcess);
			while (m.find()) 
			{			
				email.add(m.group().toLowerCase().trim());					
			}

			if(!email.isEmpty()) //if emailId is present
			{
				for(String e1:email)
				{
					if(!e1.equals("")) // checking emailID if it null
					{
						emailId+=e1+",";
						//System.out.println("inside"+emailId);
					}
				}
				if(!emailId.isEmpty())
				{
					emailId = emailId.substring(0,emailId.length()-1); //Remove the last comma(,)
				}
			}
		}
		catch(Exception e)
		{
			
		}
		//System.out.println("!!!!!!!!!!!!!!!!!"+emailId);
		return emailId;
	}
	
	public String getPhone(String textProcess)
	{
		String phone="";
		Set<String> phn = new LinkedHashSet<String>();
		// Regex for phone
		Matcher m1 = Pattern.compile("\\D*:?\\+?[ 0-9. ()-]{10,25}").matcher(textProcess);
		try
		{
			while (m1.find()) 
			{
				String num=m1.group().replaceAll("[^0-9]","").trim();// Remove the starting and ending spaces only take phone number others are remove like(,.;:'"?/()*+-_%$#!=))
				if(num.length() <= 15 && num.length() >= 10) //constraints for phone number
				{
					phn.add(num);
				}
			}
			if(!phn.isEmpty()) //if the phone number is present
			{
				for(String p:phn)
				{
					phone+=p+","; 
				}
				phone= phone.substring(0, phone.length()-1);  //Remove the last comma(,)
			}			
		}
		catch(Exception e)
		{
			
		}		
		return phone;
	}
	
	void getRecordsToProcess()
	{		
		DBObject present;
		DBCursor cursor;
		
		cursor = crawldata.find(new BasicDBObject("status", "false").append("authorityStatus", "false").append("link", Pattern.compile("pdf$", Pattern.CASE_INSENSITIVE)));
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		
		System.out.println("Current data present is " + cursor.count());
		double loads[];
		try 
		{	
			BasicDBObject result;
			BasicDBObject duplicateChk;
			while (cursor.hasNext()) 
			{
				loads=getLoadAvg();
				if(loads[0]<1.50)  // Check load Average < 1.5 
				{
					present = cursor.next();
					System.out.println("Link="+present.get("link").toString());
					result=parsePDF(present.get("link").toString());
					try
					{
						if(!result.get("description").toString().trim().equals(""))
						{
							try
							{										
								duplicateChk = new BasicDBObject("titleDescription", (result.get("authorityTitle").toString()+result.get("description").toString()).replaceAll("\\s*", ""));

								if (!titleDescription.find(duplicateChk).hasNext()) 
								{
									titleDescription.insert(duplicateChk);
									sampleCrawldata.update(present, new BasicDBObject("$set",result));
									crawldata.update(present, new BasicDBObject("$set",result));
									System.out.println("\n**********************************************************\n");
								}
								else
								{
									System.out.println("Already present in titledescription");
									sampleCrawldata.update(present, new BasicDBObject("$set",new BasicDBObject("authorityStatus", "duplicate")));
									crawldata.update(present, new BasicDBObject("$set",new BasicDBObject("authorityStatus", "duplicate")));
									System.out.println("\n**********************************************************\n");
								}	
							}
							catch(Exception e)
							{
								System.out.println("Exception occurred in checking titledescription duplicate or inserting in crawldata");
								System.out.println("\n**********************************************************\n");
							}
						}
						else
						{
							sampleCrawldata.update(present, new BasicDBObject("$set",new BasicDBObject("authorityStatus", "exception")));
							crawldata.update(present, new BasicDBObject("$set",new BasicDBObject("authorityStatus", "exception")));
							System.out.println("\n**********************************************************\n");
						}
					}
					catch(Exception e)
					{
						sampleCrawldata.update(present, new BasicDBObject("$set",new BasicDBObject("authorityStatus", "exception")));
						crawldata.update(present, new BasicDBObject("$set",new BasicDBObject("authorityStatus", "exception")));
						System.out.println("\n**********************************************************\n");
					}
				}
				else  // When cpu load average exceed 1.5 code will terminate.
				{
					System.out.println("So tired. I'm Shuting Nowwwwwww........"+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date())));
					System.exit(123);	// return status 123 to shell script indicating abnormal termination.			
				}
			}			
			cursor.close();
			System.gc();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	// This method will get current load average of CPU using "uptime" command.
		double[] getLoadAvg()
		{
			StringBuffer output = new StringBuffer();
			double loads[]=new double[3];
	        Process p;
	        try {
	                p = Runtime.getRuntime().exec("uptime"); // execute "uptime" command on linux only.
	                p.waitFor();
	                BufferedReader reader =
	                    new BufferedReader(new InputStreamReader(p.getInputStream()));

	                String line = "";
	                while ((line = reader.readLine())!= null) {
	                     output.append(line + "\n");  //Output of command
	                }

	        } catch (Exception e) {
	                e.printStackTrace();
	        }
	        
	        
	        String str=output.toString();
	        str=str.split("load average[s:][: ]")[1];  //To get only load avg from command output
	        System.out.println("Load Average= "+str);
	        String allValues[]=str.split(",");
	        loads[0]=Double.parseDouble(allValues[0].trim()); //last one Min
	        loads[1]=Double.parseDouble(allValues[1].trim()); //last five Min
	        loads[2]=Double.parseDouble(allValues[2].trim()); //last fifteen Min
	       // System.out.println(lastOneMin+"**"+lastFiveMin+"**"+lastFifteenMin+"**");
	        return loads;

		}
	
	public static void main(String[] args) throws InterruptedException 
	{
		PDFParsing object=new PDFParsing();
		object.getRecordsToProcess();
		object.mongoClient.close();
        object=null;
        System.gc();
        System.out.println("Done");
		System.exit(0);	// return status 0 to shell script indicating normal termination.
		//new PDFParsing().parsePDF("http://boa.sd.gov/divisions/procurement/contracts/carpet/NJPACarpet.pdf");
		
	}
}

