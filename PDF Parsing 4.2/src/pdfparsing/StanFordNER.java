package pdfparsing;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.text.WordUtils;

import java.util.Set;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.Triple;

public class StanFordNER {
	static String content;
	static String serializedClassifier;
	static AbstractSequenceClassifier<CoreLabel> classifier = null;
	String date, location, organization, person, percent, time, money;
	static {
		serializedClassifier = "classifiers/english.muc.7class.distsim.crf.ser.gz";

		try {
			classifier = CRFClassifier.getClassifier(serializedClassifier);
		} catch (ClassCastException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public StanFordNER() {

	}

	public HashMap<String, Set<String>> getDataForAllClasses(String content) 
	{
		HashMap<String, Set<String>> hm = new HashMap<String, Set<String>>();
		Set<String> locationAl = new LinkedHashSet<String>();
		Set<String> organizationAl = new LinkedHashSet<String>();
		Set<String> dateAl = new LinkedHashSet<String>();
		Set<String> moneyAl = new LinkedHashSet<String>();
		Set<String> personAl = new LinkedHashSet<String>();
		Set<String> percentAl = new LinkedHashSet<String>();
		Set<String> timeAl = new LinkedHashSet<String>();

		
		List<Triple<String, Integer, Integer>> list = classifier.classifyToCharacterOffsets(content);
		for (Triple<String, Integer, Integer> item : list) 
		{
			if (item.first().contains("DATE")) {
				date = content.substring(item.second(), item.third());
				dateAl.add(date);
			} else if (item.first().contains("LOCATION")) {
				location = content.substring(item.second(), item.third());
				locationAl.add(location);
			} else if (item.first().contains("ORGANIZATION")) {
				organization = content.substring(item.second(), item.third());
				organizationAl.add(organization);
			} else if (item.first().contains("PERSON")) {
				person = content.substring(item.second(), item.third());
				personAl.add(person);
			} else if (item.first().contains("PERCENT")) {
				percent = content.substring(item.second(), item.third());
				percentAl.add(percent);
			} else if (item.first().contains("TIME")) {
				time = content.substring(item.second(), item.third());
				timeAl.add(time);
			} else if (item.first().contains("MONEY")) {
				money = content.substring(item.second(), item.third());
				moneyAl.add(money);
			}

		}
		hm.put("location", locationAl);
		hm.put("organization", organizationAl);
		hm.put("date", dateAl);
		hm.put("money", moneyAl);
		hm.put("person", personAl);
		hm.put("percent", percentAl);
		hm.put("time", timeAl);
		return hm;

	}

	public static void main(String s[]) {
		/*StringBuilder sbr = new StringBuilder();
		for (int i = 0; i < s.length; i++) {
			sbr.append(s[i] + " ");
		}
		// System.out.println(sbr.toString());
		// String str="my name is ruksad, my name is ruksad";
*/		StanFordNER stanford = new StanFordNER();
		String str="0 applications Apply now Freelance Graders are required to work for a number of labels, suppliers and high street brands in London! I am looking for experienced Graders, in particular with an excellent working knowledge of the Gerber and/or Vetigraph system for freelance work throughout the year. As a freelance Grader, you will be available to work with notice to cover holidays, days off, sickness and busy periods. We are currently working with a variety of clients looking for Graders who: *Understand the grading rules *Have an excellent working knowledge of CAD systems - in particular Gerber and Vetigraph (but other systems will be considered) *Can lead a team to ensure completion of all patterns to production standards *Are experienced costers *Can produce spec charts for the manufacturer *Can confidently grade all patterns The ideal candidate will be a highly experienced Grader, with a proven track record of working effectively in a fast paced environment. You will be CAD trained, and have used systems such as Gerber and Vetigraph consistently in industry. We are looking for freelance Graders available for holiday and adhoc cover, as well as block bookings to support during busy periods. Please get in touch to register your details for future opportunities now! May and Stephens acts as an ''employment agency'' in relation to this vacancy. EQUAL OPPORTUNITIES May and Stephens Recruitment is committed to equal opportunities and actively seeks applications from all sectors of the community irrespective of gender, race, colour, nationality, ethnic or national origin, disability, marital status, having responsibility for dependents, age, religion/beliefs, or any other reason which cannot be shown to be justified. May & Stephens Ltd is acting as an Employment Business in relation to this vacancy. Apply now Email job Reference: 30946883 Bank or payment details should not be provided when applying for a job. reed.co.uk is not responsible for any external website content. All applications should be made via the 'Apply now' button.";
		str=WordUtils.capitalize(str.toLowerCase());
		System.out.println("O/P="+str);
		HashMap hm = stanford.getDataForAllClasses(str);

		Set set = hm.entrySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, Set<String>> me = (Entry<String, Set<String>>) iterator.next();
			String key = me.getKey();
			System.out.print(key + "= ");
			//Set<String> ll = me.getValue();
			System.out.println(me.getValue());
			System.out.println();
		}
	}
}
