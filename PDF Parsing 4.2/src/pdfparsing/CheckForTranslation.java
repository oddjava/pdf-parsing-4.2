package pdfparsing;

import java.io.IOException;
import java.util.List;

import org.apache.tika.langdetect.OptimaizeLangDetector;
import org.apache.tika.langdetect.TextLangDetector;
import org.apache.tika.language.LanguageIdentifier;
import org.apache.tika.language.detect.LanguageDetector;
import org.apache.tika.language.detect.LanguageResult;

public class CheckForTranslation 
{
	private static final String TRUE="true";
	private static final String FALSE="false";
	private static final String ENGLISH="en";
	
   /* public static void main( String[] args )
    {
    	//this is deprecated class
    	LanguageIdentifier identifier = new LanguageIdentifier("地域�?�検索�?果�?��?��?�ん");
        String language = identifier.getLanguage();
        System.out.println("Language of the given content is : " + language);
     
        
        //Above LanguageIdentifier class is replaced by now LanguageDetector 
        try {
        	//LanguageDetector languageDetector= new TextLangDetector();
        	LanguageDetector languageDetector=  new OptimaizeLangDetector().loadModels();
        	List<LanguageResult> languageList= languageDetector.detectAll("地域�?�検索�?果�?��?��?�ん"
		    		+ "I have collected a few articles from our blog that you might find interesting:"
		    		+ "In-Place editing with MongoChef"
		    		+ "地域�?�検索�?果�?��?��?�ん"
		    		+ "Tutorial for map-reduce queries with MongoChef"
		    		+ "Tutorial for the MongoDB aggregation pipeline"
		    		+ "BTW: Did you already give our built-in IntelliShell a go? IntelliShell offers full MongoDB shell experience with rich auto-completion."
		    		+ "Learn more about IntelliShell."
		    		+ "Do you need priority support for you and your team? We have got you covered!"
		    		+ "");
        	
        	for(LanguageResult languageResult:languageList)
        	{
        		 System.out.println("Language of the given content is : " +languageResult.getLanguage());
        	}
        	
		    LanguageResult languageResult=languageDetector.detect("地域�?�検索�?果�?��?��?�ん"
		    		+ "I have collected a few articles from our blog that you might find interesting:"
		    		+ "In-Place editing with MongoChef"
		    		+ "Working with side-by-side views"
		    		+ "Tutorial for map-reduce queries with MongoChef"
		    		+ "Tutorial for the MongoDB aggregation pipeline"
		    		+ "BTW: Did you already give our built-in IntelliShell a go? IntelliShell offers full MongoDB shell experience with rich auto-completion."
		    		+ "Learn more about IntelliShell."
		    		+ "Do you need priority support for you and your team? We have got you covered!"
		    		+ "");  //languageDetector.detect("Alla människor är födda fria och lika i värde och rättigheter.");
		    System.out.println("Language of the given content is : " +languageResult.getLanguage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
      
       
    }
    */
	
    public static String detectLanguage(String text)
    {
    	if(text.trim().equals(""))
    	{
    		return TRUE;
    	}	
    	
    	try {
			LanguageDetector languageDetector=  new OptimaizeLangDetector().loadModels();
		    LanguageResult languageResult=languageDetector.detect(text);  //languageDetector.detect("Alla människor är födda fria och lika i värde och rättigheter.");
			
			if(languageResult.getLanguage().equals(ENGLISH))
			{
				return TRUE;
			}	
			   return FALSE;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return FALSE;
		}
    }
}


/*af Afrikaans
an Aragonese
ar Arabic
ast Asturian
be Belarusian
br Breton
ca Catalan
bg Bulgarian
bn Bengali
cs Czech
cy Welsh
da Danish *
de German *
el Greek *
en English *
es Spanish *
et Estonian
eu Basque
fa Persian
fi Finnish *
fr French *
ga Irish
gl Galician
gu Gujarati
he Hebrew
hi Hindi
hr Croatian
ht Haitian
hu Hungarian
id Indonesian
is Icelandic
it Italian *
ja Japanese *
km Khmer
kn Kannada
ko Korean
lt Lithuanian *
lv Latvian
mk Macedonian
ml Malayalam
mr Marathi
ms Malay
mt Maltese
ne Nepali
nl Dutch *
no Norwegian
oc Occitan
pa Punjabi
pl Polish
pt Portuguese *
ro Romanian
ru Russian
sk Slovak
sl Slovene
so Somali
sq Albanian
sr Serbian
sv Swedish *
sw Swahili
ta Tamil
te Telugu
th Thai *
tl Tagalog
tr Turkish
uk Ukrainian
ur Urdu
vi Vietnamese
yi Yiddish
zh-CN Simplified Chinese * (just generic Chinese)
zh-TW Traditional Chinese * (just generic Chinese)*/
