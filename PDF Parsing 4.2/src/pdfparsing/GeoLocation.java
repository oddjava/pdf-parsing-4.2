package pdfparsing;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;



public class GeoLocation
{
	public static String[] updateLocation(String alllocation) 
	{
		
		String allLatlong = "",latlong="";
		String outputGeo[]=new String[2]; //return after method is finished.
		try
		{	
			// Get Unique set of location.
			Set<String> locationset = new LinkedHashSet<String>();
			DBCursor cursor=null;
			DBObject present=null;			
			
			//Split location by comma and add to set
			String[] locations = alllocation.split(",");
			for (int i = 0; i < locations.length; i++) 
			{
				if(!locations[i].trim().equals(""))
				{
					locationset.add(locations[i].trim());
				}
			}
			
			// JSON String for geoip.
			String query = "'coordinates' : [ ";
				
			//If location is empty
			if(alllocation.trim().equals("") || alllocation.trim().equals(",") )
			{
				latlong="0.0,0.0";
				allLatlong = allLatlong + latlong + "/";
				query += "[ 0.0, 0.0 ]";
			}
			else // If location is present.
			{	
			  int count=0;
			  int i;
			  String lon[] ;
			  // Traverse location added to set
			  for (String location : locationset) 
			  {
				//System.out.println("Found location="+location); 
				location=StringUtils.stripAccents(location);			
				location= location.replaceAll("[-+.^:,|]","").trim();
				try
				{
					//System.out.println(new BasicDBObject("cityname", Pattern.compile("^" + location + "$", Pattern.CASE_INSENSITIVE)).toJson());
					cursor = PDFParsing.geodata.find(new BasicDBObject("cityname", Pattern.compile("^" + location + "$", Pattern.CASE_INSENSITIVE)));	//If we don not have query then keep brackets blank. It will fetch all records
					cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);	
				}
				catch(Exception e)
				{}
				if(cursor==null || !cursor.hasNext()) //If location not present in geoLocation DB.
				{
					latlong="0.0,0.0";
					allLatlong = allLatlong + latlong + "/";
					query += "[ 0.0, 0.0 ]";
				}
				else //If found.
				{			
					i=0;
					lon=null;
					 // Add geo coordinate of each location to JSON String
					while (cursor.hasNext()) //most of the time only one location is found. but still for safer-side i used while loop.
					{
						present=cursor.next();
						latlong=present.get("latlong").toString();
						
						//LatLong stored in DB is seperated by comma so split them.
						lon = latlong.split(",");
						
						if (i == cursor.size() - 1) 
						{
							query += "[" + lon[1] + "," + lon[0] + "]";							
						} 
						else
						{
							query += "[" + lon[1] + "," + lon[0] + "],";
							i++;
						}
						
						allLatlong = allLatlong + latlong + "/"; // String of Geo coordinates seperated by slash.
					}
				}
				
				if(count<locationset.size()-1)
				{
					query += ",";
				}
				count++;
				
			  }
			}
			query += "]";
			
			// Complete JSON String
			query="{" + "'type' : 'MultiPoint'," + query + "" + "}";
			
			outputGeo[0]=allLatlong; // String of Geo coordinates.
			outputGeo[1]=query;      // JSON String of Geo coordinates.

		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return outputGeo; // return array.
	}
	
	public static void main(String args[])
	{
		String outputGeo[]=updateLocation("Auto Failover, Etc");
		System.out.println("geoLocation="+outputGeo[0]+"\ngeoip="+outputGeo[1]);
	}
}
